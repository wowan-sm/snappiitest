package vladimirv.com.test.model;

/**
 * Created by Vladimir Varavin on 03.10.18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Name implements Serializable {
    @SerializedName("first")
    @Expose
    private String first;

    @SerializedName("last")
    @Expose
    private String last;

    @Override
    public String toString() {
        return first + " " + last;
    }
}