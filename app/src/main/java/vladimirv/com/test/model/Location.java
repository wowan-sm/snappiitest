package vladimirv.com.test.model;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vladimir Varavin on 03.10.18.
 */
public class Location implements Serializable {

    @SerializedName("street")
    @Expose
    private String street;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("state")
    @Expose
    private String state;

    @SerializedName("postcode")
    @Expose
    private String postcode;

    @Override
    public String toString() {
        return TextUtils.join(", ", new String[]{postcode, state, city, street});
    }
}