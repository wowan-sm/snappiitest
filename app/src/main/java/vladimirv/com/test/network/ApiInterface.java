package vladimirv.com.test.network;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import vladimirv.com.test.model.Result;

public interface ApiInterface {

    @GET("api/")
    Call<Result> getUsers(@QueryMap(encoded = true) Map<String, String> filters);
}